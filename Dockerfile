FROM node:alpine3.12
WORKDIR /server
COPY package.json .
RUN ["npm", "install"]
COPY . .

ENV PORT 4444
CMD ["node", "server.js" ]